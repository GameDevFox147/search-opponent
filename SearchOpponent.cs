﻿using System;
using Bazaar.Protobuf.Shared;
using Cysharp.Threading.Tasks;
using Framework.Core.Types;
using Framework.Logger;
using Scripts.Gameplay.Interaction.Filters;
using Scripts.Gameplay.Model.Fight;
using Scripts.Gameplay.Model.Fight.Board;
using Scripts.Gameplay.Model.Fight.Board.Bazaar;
using UnityEngine;
using UnityEngine.Assertions;

namespace Scripts.UI.Screens.Fight.SearchOpponent
{
    public class SearchOpponent : ContextDataMonoBehaviour<FightModel>
    {
        private readonly int StartSearch = Animator.StringToHash("StartSearch");
        private readonly int OpponentFounded = Animator.StringToHash("OpponentFounded");
        private readonly int SelectOpponent = Animator.StringToHash("SelectOpponent");
        private readonly int SelectOpponentPrepared = Animator.StringToHash("SelectOpponentPrepared");

#pragma warning disable 649
        [SerializeField] private Animator animator;
        [SerializeField] private CanvasGroup canvasGroup;

        [Space]
        [SerializeField] private HeroViewTransition heroView;
        [SerializeField] private HeroViewTransition opponentView;
        [SerializeField] private int findOpponentAnimationTimeMS = 2000;
        [SerializeField] private int selectOpponentPreparedAnimationTimeMS = 2000;
#pragma warning restore 649

        private bool matchmaking;
        private bool boardReady;

        private void Awake()
        {
            Assert.IsNotNull(animator);
            Assert.IsNotNull(canvasGroup);
            Assert.IsNotNull(heroView);
            Assert.IsNotNull(opponentView);
        }


        private void Reset()
        {
            animator = GetComponent<Animator>();
            canvasGroup = GetComponent<CanvasGroup>();
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            var matchmaking = App.Gameplay.Services.Matchmaking;
            matchmaking.StartMatchmakingEvent += SearchOpponent;
            matchmaking.ErrorMatchmakingEvent += HideScreen;
            matchmaking.SelectOpponentEvent += ShowSelectOpponent;
            App.Gameplay.BoardView.BattleBoardInitializedEvent += OnBattleBoardInitialized;
            ContextData.FightInitialize += FindOpponent;

            HideScreen();
        }

        protected override void OnTerminate()
        {
            base.OnTerminate();

            var matchmaking = App.Gameplay.Services.Matchmaking;
            matchmaking.StartMatchmakingEvent -= SearchOpponent;
            matchmaking.ErrorMatchmakingEvent -= HideScreen;
            matchmaking.SelectOpponentEvent -= ShowSelectOpponent;
            App.Gameplay.BoardView.BattleBoardInitializedEvent -= OnBattleBoardInitialized;
            App.Gameplay.Model.Fight.FightInitialize -= FindOpponent;
        }

        private void SearchOpponent()
        {
            InteractionFilters.Disable();
            matchmaking = true;
            boardReady = false;
            App.Gameplay.Model.Replay.WaitForReplayStart = true;
            SetupFighterFromBoard(heroView, ContextData.Board.PlayerHalfBoard);
            ShowScreen();

            animator.SetTrigger(StartSearch);
        }

        private void FindOpponent()
        {
            var fighterType = ContextData.Board.OpponentHalfBoard.Fighter.FighterProto.Type;
            if (fighterType == FighterType.Monster || App.Gameplay.Model.Replay.IsRepeatReplay) return;

            if (matchmaking)
            {
                FindPlayerInMatchmaking();
            }

            matchmaking = false;
        }

        private void FindPlayerInMatchmaking()
        {
            SetupFighterFromBoard(opponentView, ContextData.Board.OpponentHalfBoard);
            animator.SetTrigger(OpponentFounded);
            FindOpponentCompleted().Forget();
        }

        private async UniTask FindOpponentCompleted()
        {
            await UniTask.Delay(findOpponentAnimationTimeMS, true);
            HideScreen();
            App.Gameplay.Model.Replay.WaitForReplayStart = false;
            InteractionFilters.Enable();
        }

        private void ShowSelectOpponent(EncounterModel encounterModel)
        {
            ShowSelectOpponentAsync(encounterModel).Forget();
        }

        private async UniTask ShowSelectOpponentAsync(EncounterModel encounterModel)
        {
            boardReady = false;
            InteractionFilters.Disable();
            SetupFighterFromBoard(heroView, ContextData.Board.PlayerHalfBoard);
            SetupFighterFromPlayerEncounter(opponentView, encounterModel);

            App.Gameplay.Model.Replay.WaitForReplayStart = true;
            ShowScreen();
            animator.SetTrigger(SelectOpponent);
            // Wait select opponent animation
            await UniTask.Delay(1333, true);
            // Load delay
            await UniTask.Delay(1000, true);

            await UniTask.WaitUntil(() => boardReady);

            animator.SetTrigger(SelectOpponentPrepared);
            await UniTask.Delay(selectOpponentPreparedAnimationTimeMS, true);
            HideScreen();
            App.Gameplay.Model.Replay.WaitForReplayStart = false;
            InteractionFilters.Enable();
        }

        private void ShowScreen()
        {
            SetStateScreen(true);
        }

        private void HideScreen()
        {
            SetStateScreen(false);
        }

        private void SetStateScreen(bool state)
        {
            canvasGroup.alpha = state ? 1f : 0f;
            canvasGroup.interactable = !state;
            canvasGroup.blocksRaycasts = state;
        }

        private void SetupFighterFromBoard(HeroViewTransition fighterViewTransition, HalfBoardModel halfBoard)
        {
            var fighter = halfBoard.Fighter;
            var accountId = fighter.FighterProto.AccountId;
            var environment = App.Model.Room.EnvironmentProto.GetEnvironmentProto(accountId);

            if (environment == null)
            {
                Printer.Error(LogLayer.General, $"Empty environment for fighter, fill default data");
                environment = EnvironmentProto.DefaultPlayerEnvironment;
            }

            SearchScreenHeroModel searchScreenFighter = new SearchScreenHeroModel
            {
                NickName = environment.Name,
                FighterTemplateId = fighter.FighterProto.TemplateId,
                FighterSkinTemplateId = environment.HeroSkinTemplateId,
                RankPoints = environment.Rank.Points
            };

            fighterViewTransition.Initialize(searchScreenFighter);
        }

        private void SetupFighterFromPlayerEncounter(HeroViewTransition fighterViewTransition, EncounterModel encounterModel)
        {
            var proto = encounterModel.Proto;
            var player = proto.Player;

            SearchScreenHeroModel searchScreenFighter = new SearchScreenHeroModel
            {
                NickName = player.Name,
                FighterTemplateId = proto.TemplateId, // in this case template id on encounter is template id on fighter template
                FighterSkinTemplateId = player.HeroSkinTemplateId,
                RankPoints = player.Rank.Points
            };
            fighterViewTransition.Initialize(searchScreenFighter);
        }

        private void OnBattleBoardInitialized()
        {
            boardReady = true;
        }
    }
}